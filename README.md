# Battle Of Chancellorsville

An artilleryman of New York’s First Artillery needs your help.

Find all the details here https://adamseew.bitbucket.io/teaching/optimization-control/battle-chancellorsville/ and have fun with optimization!

If you like to know more about what's happening in depth attend also the other lectures; meantime search for "indirect methods for optimization problems" or ping me in the office!
