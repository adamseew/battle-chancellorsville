#!/bin/bash

echo "[ ] compiling..."
g++ program.cpp src/vectorn.cpp src/integrator_rkn.cpp src/integrator_rk4.cpp src/wooden_ball.cpp src/cannon_ball.cpp src/solver_shooting.cpp -std=c++11
echo "[x] compiled"

OPTION=$(whiptail --title "Optimering og Kontrol" --menu "choose an example from the list" 14 40 3 \
    "1" "  wooden ball  " \
    "2" "  cannon ball  " \
    "3" "shooting method"  3>&2 2>&1 1>&3
)

exitstatus=$?
if [ $exitstatus = 0 ]; then
    case $OPTION in
        1)
            whiptail --title "wooden ball" --msgbox "Example description: a simple example of a thrown wooden ball, which involves only y axis with different initial velocities. Matlab is used to plot results." 14 40
            echo "[ ] running wooden ball..."
            echo "[ ] saving data..."
            for i in {45..120..5}
            do
                ./a.out 0 $i > .tmp_$i.dat
            done
            echo -e "[1] \e[4mdone. data saved\e[0m"
            ;;
        2)
            whiptail --title "Cannon ball" --msgbox "Example description: a simple cannon ball example with x,y axis and varying angles of fire. Either drag and initial velocity is considered to model the problem. Matlab is used to plot results." 14 40
            echo "[ ] running cannon ball..."
            echo "[ ] saving data..."
            for i in {25..95..5}
            do
                ./a.out 1 $i > .tmp_$i.dat
            done
            echo -e "[2] \e[4mdone. data saved\e[0m"
            ;;
        3)
            whiptail --title "Shooting method" --msgbox "Example description: a complex shooting method solver example, that represents a system of a flying cannon projectile, that uses modified bisection method to adjust parameters. The same ODE of cannon ball example is considered. Matlab is used to plot results." 14 40

            OPTION2=$(whiptail --title "Shooting method precision" --menu "Choose a desired precision" 14 73 2 \
                "N" "    Regular precision: reach a goal distant 10^3 km whithin 500 m" \
                "E" "Extremely high precision: spots a target distant 10^3 km of size  1 m"  3>&2 2>&1 1>&3	
            )

            echo "[ ] running shooting method on cannon ball..."
            echo "[ ] saving data..."
            case $OPTION2 in
                E)
                    echo "[!] warning: the solver may take several minutes!"
                    echo -e "[ ] \e[7mh=0.0001, epsilon=0.001\e[0m"
                    echo "[>] shooting method with bisection started"
                    count=0
                    ./a.out 2 45 1 |
                    stdbuf -oL tr -s '\r' '\n' |
                    while read -r str
                    do
                        echo $(($count*5))
                        ((count++))
                    done | whiptail --gauge "solver is performing (yes, will take a bit)" 6 50 0 
                    echo -e "[x] \e[4msolved!\e[0m"
                    ;;
                *)
                    echo -e "[ ] \e[7mh=0.01, epsilon=0.5\e[0m"
                    echo "[>] shooting method with bisection started"
                    count=0
                    ./a.out 2 45 |
                    stdbuf -oL tr -s '\r' '\n' |
                    while read -r str
                    do
                        echo $(($count*10))
                        ((count++))
                    done | whiptail --gauge "solver is performing..." 6 50 0 
                    echo -e "[x] \e[4msolved!\e[0m"
                    ;;
            esac
            echo -e "[3] \e[4mdone. data saved\e[0m"
            ;;
    esac
    
    echo "[ ] starting matlab..."
    echo -e "[!] \e[1malways type 'quit' to exit matlab\e[0m"
    matlab -nodesktop -nosplash -r "programtype=$OPTION;run plotdata.m;"
fi

echo "[ ] cleaning..."
rm .tmp_*
rm a.out
echo "[x] cleaned"
echo -e "[x] \e[4mdone!\e[0m"
